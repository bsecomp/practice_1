import React from 'react';

const UserInput = (props) => {
  return (
    <div className="userInput">
      <input type="text" onChange={props.changeMusic} value={props.value}/>
    </div>
  )
}


export default UserInput;