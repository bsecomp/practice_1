import React from 'react';
import './UserOutput.css';

const UserOutput = (props) => {
  return (
    <div className="userOutput">
      <p>{props.band}</p>
      <p>{props.music}</p>
    </div>
  )
}

export default UserOutput;