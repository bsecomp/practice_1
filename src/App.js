import React, { Component } from 'react';
import './App.css';
import UserOutput from './UserOutput/UserOutput'
import UserInput from './UserInput/UserInput'


class App extends Component{

  state = {
    musics: [
      {band:"Queen", music:"Don't try so hard"},
      {band:"David Bowie", music:"China Girl"},
      {band:"Hugh Laurie", music:"Swanner River"},
      {band:"Beatles", music:"Oh! Darling"},
    ]
  }

  changeMusic = () =>{
    this.setState({
      musics: [
        {band:"Queen", music:"The Show Must Go On"},
        {band:"David Bowie", music:"Space Oddity"},
        {band:"Hugh Laurie", music:"Let Them Talk"},
        {band:"Beatles", music:"Bluebird"},
      ]
    })
  }

  changeMusicInput = (event) =>{
    this.setState({
      musics: [
        {band:"Queen", music: event.target.value},
        {band:"David Bowie", music:"Space Oddity"},
        {band:"Hugh Laurie", music:"Let Them Talk"},
        {band:"Beatles", music:"Bluebird"},
      ]
    })
  }


  render(){
    const style = {
      backgroundColor: 'plum',
      font:'inherit',
      border:'1px solid plum',
      padding: '10px',
      marginTop: '10px',
      color: 'black',
      cursor:'pointer',
      borderRadius: '20px',
      textTransform:'uppercase'
    }


    return (
      <div className="App">
        <button style={style} onClick={this.changeMusic}> Switch Music </button>
        <UserOutput band={this.state.musics[0].band} music={this.state.musics[0].music} />
        <UserOutput band={this.state.musics[1].band} music={this.state.musics[1].music} />
        <UserOutput band={this.state.musics[2].band} music={this.state.musics[2].music} />
        <UserOutput band={this.state.musics[3].band} music={this.state.musics[3].music} />

        <div className="newMusic">
          <UserInput changeMusic={this.changeMusicInput} value={this.state.musics[0].music}/>
        </div>
      </div>
    );
  }
}

export default App;
